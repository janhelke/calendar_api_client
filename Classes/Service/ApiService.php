<?php
declare(strict_types=1);

namespace JanHelke\CalendarApiClient\Service;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Calender API client service
 */
class ApiService
{
    public array $configuration = [];

    public function __construct()
    {
        $this->configuration = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['calendar_api_client'];
    }

    /**
     * @param string $apiKey
     * @param array $parameters
     * @return string
     */
    public function call(string $apiKey, array $parameters = []): string
    {
        $queryParameter = $this->getQueryParameter($apiKey, $parameters);
        if (VersionNumberUtility::convertVersionNumberToInteger('TYPO3_version') >= 9_002_000) {
            $query = HttpUtility::buildQueryString($queryParameter);
        } else {
            $query = self::buildQueryString($queryParameter);
        }

        return $this->getApiServerUrl($query);
    }

    /**
     * @param string $query
     * @return string
     */
    protected function getApiServerUrl(string $query = ''): string
    {
        $urlParts =
            [
                'scheme' => $this->configuration['server']['scheme'] ?? 'https',
                'host' => empty($this->configuration['server']['host']) ? GeneralUtility::getIndpEnv('HTTP_HOST') : $this->configuration['server']['host'],
                'path' => $this->configuration['server']['path'] ?? '/index.php',
                'query' => $query
            ];

        if (!empty($this->configuration['server']['user'])) {
            $urlParts['user'] = $this->configuration['server']['user'];
        }

        if (!empty($this->configuration['server']['pass'])) {
            $urlParts['pass'] = $this->configuration['server']['pass'];
        }

        if (!empty($this->configuration['server']['port'])) {
            $urlParts['port'] = $this->configuration['server']['port'];
        }

        return HttpUtility::buildUrl($urlParts);
    }

    /**
     * @param string $apiKey
     * @param array $parameters
     * @return array
     */
    protected function getQueryParameter(string $apiKey, array $parameters): array
    {
        $queryParameter = [
            'type' => $this->configuration['server']['type'] ?? '1562612382'
        ];
        foreach ($parameters as $key => $value) {
            $queryParameter['tx_calendarapi_' . $apiKey][$key] = $value;
        }

        return $queryParameter;
    }

    /**
     * Implodes a multidimensional array of query parameters to a string of GET parameters (eg. param[key][key2]=value2&param[key][key3]=value3)
     * and properly encodes parameter names as well as values. Spaces are encoded as %20
     *
     * @param array $parameters The (multidimensional) array of query parameters with values
     * @param string $prependCharacter If the created query string is not empty, prepend this character "?" or "&" else no prepend
     * @param bool $skipEmptyParameters If true, empty parameters (blank string, empty array, null) are removed.
     * @return string Imploded result, for example param[key][key2]=value2&param[key][key3]=value3
     * @see explodeUrl2Array()
     * @deprecated This function is deprecated since TYPO3 9 LTS has the same function called HttpUtility::buildQueryString
     */
    protected static function buildQueryString(
        array $parameters,
        string $prependCharacter = '',
        bool $skipEmptyParameters = false
    ): string {
        if (empty($parameters)) {
            return '';
        }

        if ($skipEmptyParameters) {
            // This callback filters empty strings, array and null but keeps zero integers
            $parameters = ArrayUtility::filterRecursive(
                $parameters,
                fn ($item) => $item !== '' && $item !== [] && $item !== null
            );
        }

        $queryString = http_build_query($parameters, '', '&', PHP_QUERY_RFC3986);
        $prependCharacter = $prependCharacter === '?' || $prependCharacter === '&' ? $prependCharacter : '';

        return $queryString && $prependCharacter ? $prependCharacter . $queryString : $queryString;
    }
}
