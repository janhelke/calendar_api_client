<?php
declare(strict_types=1);

namespace JanHelke\CalendarApiClient\Tests\Unit\Service;

use JanHelke\CalendarApiClient\Service\ApiService;
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Dummy test
 */
class ApiServiceUnitTest extends UnitTestCase
{

    /**
     * @return array
     */
    public function callDataProvider(): array
    {
        // Please note: In a CLI environment (like hte unit tests) the function
        // GeneralUtility::getIndpEnv('HTTP_HOST') will return null. Thus the
        // strange looking expectedUrl.
        return [
            'empty' => [
                'configuration' => [
                    'server' => []
                ],
                'apiKey' => '',
                'parameters' => [],
                'expectedUrl' => 'https:///index.php?type=1562612382'
            ],
            'apiKey with no parameters' => [
                'configuration' => [
                    'server' => []
                ],
                'apiKey' => 'fooApi',
                'parameters' => [],
                'expectedUrl' => 'https:///index.php?type=1562612382'
            ],
            'apiKey with parameters' => [
                'configuration' => [
                    'server' => []
                ],
                'apiKey' => 'fooApi',
                'parameters' => [
                    'foo' => 'bar',
                    'baz' => '123'
                ],
                'expectedUrl' => 'https:///index.php?type=1562612382&tx_calendarapi_fooApi%5Bfoo%5D=bar&tx_calendarapi_fooApi%5Bbaz%5D=123'
            ],
            'custom configuration' => [
                'configuration' => [
                    'server' => [
                        'scheme' => 'http',
                        'user' => 'foo',
                        'pass' => 'bar',
                        'port' => '8080',
                        'host' => 'baz',
                        'path' => '/foobar.php',
                        'type' => '12345'
                    ]
                ],
                'apiKey' => 'fooApi',
                'parameters' => [],
                'expectedUrl' => 'http://foo:bar@baz:8080/foobar.php?type=12345'
            ],
            'custom configuration with apiKey and parameters' => [
                'configuration' => [
                    'server' => [
                        'scheme' => 'http',
                        'user' => 'foo',
                        'pass' => 'bar',
                        'port' => 8080,
                        'host' => 'baz',
                        'path' => '/foobar.php',
                        'type' => 12345
                    ]
                ],
                'apiKey' => 'fooApi',
                'parameters' => [
                    'foo' => 'bar',
                    'baz' => '123'
                ],
                'expectedUrl' => 'http://foo:bar@baz:8080/foobar.php?type=12345&tx_calendarapi_fooApi%5Bfoo%5D=bar&tx_calendarapi_fooApi%5Bbaz%5D=123'
            ],
            'empty path configuration' => [
                'configuration' => [
                    'server' => [
                        'path' => ''
                    ]
                ],
                'apiKey' => 'fooApi',
                'parameters' => [],
                'expectedUrl' => 'https://?type=1562612382'
            ],
        ];
    }

    /**
     * @param array $configuration
     * @param string $apiKey
     * @param array $parameters
     * @param string $expectedUrl
     * @dataProvider callDataProvider
     */
    public function testCall(array $configuration, string $apiKey, array $parameters, string $expectedUrl): void
    {
        $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['calendar_api_client'] = array_merge(
            [
                'server' => [
                    'scheme' => 'https',
                    'type' => '1562612382',
                    'path' => 'index.php'
                ]
            ],
            $configuration
        );
        self::assertEquals($expectedUrl, (new ApiService())->call($apiKey, $parameters));
    }
}
