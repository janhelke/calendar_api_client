<?php

$EM_CONF['calendar_api_client'] = [
    'title' => 'Calendar API client side',
    'description' => 'This is the client side of the calendar API. All calls to the SPI should be routed thru here.',
    'category' => 'be',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0 - 10.9.99'
        ],
    ]
];
