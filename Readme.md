# TYPO3 Extension calendar_api_client
This extension provides all necessary functions to connect your custom calendar frontend to the calendar_framework on the server

**Please note:** This is currently a very rudimentary approach to connect to the server via a type number and over https. Upcoming versions of the api will use the TYPO3 middleware (available from TYPO3 9 LTS on) and will also provide more encryption.

## Configuration
The extension can be configured globally using the extension settings in the extension manager. If you run the calendar_foundation, calendar_api, calendar_api_client and your frontend extension on the same server (that, to be honest, is the most likely use case) you don't need to configure anything.

If you host the calendar_foundation and the calendar_api on a different server, you need to adjust the configuration.

**scheme**: This only needs to be set, if your server doesn't understand https. You really should invest more effort to enable https on your servers side but to configure this setting to "http".

**user**: If your server is protected with a htaccess / htpasswd configuration, you can set the users username here.

**pass**: If your server is protected with a htaccess / htpasswd configuration, you can set the users password here.

**port**: If your server doesn't listen to the default ports, you can switch the port here. Please keep in mind, that different schemes connect to different ports.

**host**: This is the host address of your server without scheme and following path. In a non-exotic server-client setup this should be the only value you need to configure.

**type**: If your server is for reasons unknown configured to listen to a different typeNum than 1562612382, you can configure this here.

**path**: If your servers TYPO3 doesn't use the default /index.php (e.g. because your TYPO3 is installed in a subdirectory) you can configure the path here.
